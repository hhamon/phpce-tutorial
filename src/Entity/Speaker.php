<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SpeakerRepository")
 */
class Speaker
{
    use EntityIdentityTrait;

    /**
     * @ORM\Column(length=50)
     *
     * @Assert\NotBlank(message="Speaker name is required.")
     * @Assert\Length(min=2, max=50)
     */
    private $name;

    /**
     * @ORM\Column(type="text", nullable=true)
     *
     * @Assert\Length(min=10)
     */
    private $biography;

    /**
     * @ORM\Column(length=100, nullable=true)
     */
    private $picture;

    /**
     * @Assert\NotBlank
     * @Assert\Image(
     *   maxSize="2M",
     *   minWidth=100,
     *   maxWidth=1024
     * )
     */
    private $uploadedPicture;

    public function setUploadedPicture(UploadedFile $file): void
    {
        $this->uploadedPicture = $file;
    }

    public function getUploadedPicture(): ?UploadedFile
    {
        return $this->uploadedPicture;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getBiography(): ?string
    {
        return $this->biography;
    }

    public function setBiography(?string $biography): void
    {
        $this->biography = $biography;
    }

    public function setPicture(string $picture): void
    {
        $this->picture = $picture;
    }

    public function getPicture(): ?string
    {
        return $this->picture;
    }
}