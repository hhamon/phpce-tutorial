<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Webmozart\Assert\Assert;

/**
 * @ORM\Entity
 * @ORM\Table(uniqueConstraints={
 *  @ORM\UniqueConstraint(
 *     name="administrator_email_unique",
 *     columns="email_address"
 *   )
 * })
 */
class Administrator implements UserInterface
{
    use EntityIdentityTrait;

    /**
     * @ORM\Column(length=100)
     */
    private $emailAddress;

    /**
     * @ORM\Column
     */
    private $password;

    /**
     * @ORM\Column(type="boolean")
     */
    private $superAdmin = false;

    public static function register(string $emailAddress, string $password): self
    {
        Assert::notEmpty($emailAddress);
        Assert::lengthBetween($emailAddress, 5, 100);
        Assert::notEmpty($password);
        Assert::lengthBetween($emailAddress, 10, 255);

        $admin = new self();
        $admin->emailAddress = $emailAddress;
        $admin->password = $password;

        return $admin;
    }

    public function grantSuperAdmin(): void
    {
        $this->superAdmin = true;
    }

    public function getEmailAddress(): ?string
    {
        return $this->emailAddress;
    }

    public function setEmailAddress(string $emailAddress): void
    {
        $this->emailAddress = $emailAddress;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): void
    {
        $this->password = $password;
    }

    public function getRoles()
    {
        return $this->superAdmin ? ['ROLE_SUPER_ADMIN'] : ['ROLE_ADMIN'];
    }

    public function getSalt()
    {
    }

    public function getUsername()
    {
        return $this->emailAddress;
    }

    public function eraseCredentials()
    {
    }
}