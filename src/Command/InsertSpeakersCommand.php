<?php

namespace App\Command;

use App\Entity\Speaker;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class InsertSpeakersCommand extends Command
{
    private $doctrine;

    public function __construct(ManagerRegistry $doctrine)
    {
        parent::__construct(null);

        $this->doctrine = $doctrine;
    }

    protected function configure()
    {
        $this
            ->setName('app:speakers:import')
            ->setDescription('Import some Speakers data')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $speaker1 = new Speaker();
        $speaker1->setName('Sebastian Bergmann');
        $speaker1->setBiography('Sebastian Bergmann has instrumentally contributed to transforming PHP into a reliable platform for large-scale, critical projects. Enterprises and PHP developers around the world benefit from the tools that he has developed and the experience he shares.');

        $speaker2 = new Speaker();
        $speaker2->setName('Hugo Hamon');

        $speaker3 = new Speaker();
        $speaker3->setName('Derick Rethans');

        $em = $this->doctrine->getManager();
        $em->persist($speaker1);
        $em->persist($speaker2);
        $em->persist($speaker3);
        $em->flush();

        $output->writeln('Speakers import done!');
    }
}