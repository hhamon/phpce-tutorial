<?php

namespace App\DataFixtures\ORM;

use App\Entity\Administrator;
use App\Entity\Speaker;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;

class LoadConferenceData extends Fixture implements ContainerAwareInterface
{
    public function load(ObjectManager $em)
    {
        $admin1 = Administrator::register(
            'admin@conference.tld',
            '$2y$13$BweC5XgsTCOU7dku3dN/H.w2JmUJ0EiyDDgCBhCUthL4/uH9dN1f2'
        );

        $admin2 = Administrator::register(
            'superadmin@conference.tld',
            '$2y$13$BweC5XgsTCOU7dku3dN/H.w2JmUJ0EiyDDgCBhCUthL4/uH9dN1f2'
        );
        $admin2->grantSuperAdmin();

        $em->persist($admin1);
        $em->persist($admin2);

        $speaker1 = new Speaker();
        $speaker1->setName('Sebastian Bergmann');
        $speaker1->setBiography('Sebastian Bergmann has instrumentally contributed to transforming PHP into a reliable platform for large-scale, critical projects. Enterprises and PHP developers around the world benefit from the tools that he has developed and the experience he shares.');

        $speaker2 = new Speaker();
        $speaker2->setName('Hugo Hamon');

        $speaker3 = new Speaker();
        $speaker3->setName('Derick Rethans');

        $em->persist($speaker1);
        $em->persist($speaker2);
        $em->persist($speaker3);
        $em->flush();
    }
}