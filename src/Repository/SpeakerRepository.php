<?php

namespace App\Repository;

use App\Entity\Speaker;
use Doctrine\ORM\EntityRepository;

class SpeakerRepository extends EntityRepository
{
    public function findByMyCustomMethod(int $id): ?Speaker
    {
        die('foo');
    }

    public function findForList(): array
    {
        return $this
            ->createQueryBuilder('s')
            ->orderBy('s.name','ASC')
            ->getQuery()
            ->getResult();
    }
}