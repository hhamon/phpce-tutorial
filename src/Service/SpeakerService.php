<?php

namespace App\Service;

use App\Entity\Speaker;
use Doctrine\Common\Persistence\ObjectManager;

class SpeakerService
{
    private $uploadDirectory;
    private $objectManager;

    public function __construct(
        string $uploadDirectory,
        ObjectManager $objectManager
    ) {
        $this->uploadDirectory = $uploadDirectory;
        $this->objectManager = $objectManager;
    }

    public function save(Speaker $speaker): void
    {
        if (!$id = $speaker->getId()) {
            $this->objectManager->persist($speaker);
            $this->objectManager->flush();
        }

        if ($pic = $speaker->getUploadedPicture()) {
            $name = sprintf('speaker_%s.%s', $speaker->getId(), $pic->guessExtension());
            $pic->move($this->uploadDirectory, $name);
            $speaker->setPicture($name);
        }

        $this->objectManager->flush();
    }
}