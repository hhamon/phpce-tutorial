<?php

namespace App\Form;

use App\Entity\Speaker;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SpeakerType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class)
            ->add('biography', TextareaType::class, [
                'required' => false,
            ])
            ->add('uploadedPicture', FileType::class, [
                'required' => false,
            ])
            ->add('submit', SubmitType::class, [
                'label' => $options['edit_mode'] ? 'Edit speaker' : 'Create speaker',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'edit_mode' => false,
            'data_class' => Speaker::class,
            'csrf_field_name' => 'challenge',
        ]);
    }
}