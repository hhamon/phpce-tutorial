<?php

namespace App\Controller;

use App\Entity\Speaker;
use App\Repository\SpeakerRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class ConferenceController extends AbstractController
{
    /**
     * @Route("/", name="app_homepage", methods="GET")
     */
    public function indexAction(): Response
    {
        return $this->render('conference/index.html.twig');
    }

    /**
     * @Route("/speakers", name="app_list_speakers", methods="GET")
     */
    public function listSpeakersAction(SpeakerRepository $repository): Response
    {
        return $this->render('conference/speakers.html.twig', [
            'speakers' => $repository->findForList(),
        ]);
    }

    /**
     * @Route("/speakers/{id}", name="app_show_speaker", methods="GET", requirements={
     *   "id": "%valid_id_pattern%"
     * })
     */
    public function showSpeakerAction(Speaker $speaker): Response
    {
        return $this->render('conference/speaker.html.twig', [
            'speaker' => $speaker,
        ]);
    }
}