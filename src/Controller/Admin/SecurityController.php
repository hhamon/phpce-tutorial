<?php

namespace App\Controller\Admin;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

/**
 * @Route("/admin", name="admin_")
 */
class SecurityController extends AbstractController
{
    /**
     * @Route("/login", name="login", methods="GET")
     */
    public function loginAction(AuthenticationUtils $utils): Response
    {
        return $this->render('admin/login.html.twig', [
            'last_username' => $utils->getLastUsername(),
            'error' => $utils->getLastAuthenticationError(),
        ]);
    }

    /**
     * @Route("/auth", name="auth", methods="POST")
     */
    public function loginCheckAction(): Response
    {
    }

    /**
     * @Route("/logout", name="logout", methods="GET")
     */
    public function logoutAction(): Response
    {
    }
}