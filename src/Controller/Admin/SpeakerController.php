<?php

namespace App\Controller\Admin;

use App\Entity\Speaker;
use App\Form\SpeakerType;
use App\Service\SpeakerService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/admin/speakers", name="admin_speakers_")
 * @Security("has_role('ROLE_ADMIN')")
 */
class SpeakerController extends AbstractController
{
    /**
     * @Route("/create", name="create", methods={"GET", "POST"})
     */
    public function createAction(Request $request, SpeakerService $service): Response
    {
        $form = $this->createForm(SpeakerType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $service->save($form->getData());

            return $this->redirectToRoute('admin_speakers_create');
        }

        return $this->render('admin/speaker/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/edit/{id}", name="edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, SpeakerService $service, Speaker $speaker): Response
    {
        $form = $this->createForm(SpeakerType::class, $speaker, ['edit_mode' => true]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $service->save($speaker);

            return $this->redirectToRoute('admin_speakers_edit', ['id' => $speaker->getId() ]);
        }

        return $this->render('admin/speaker/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/delete/{id}", name="delete", methods={"GET", "POST"})
     * @Security("is_granted('ROLE_SUPER_ADMIN')")
     */
    public function deleteAction(Request $request, SpeakerService $service, Speaker $speaker): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        die('DELETE');
    }
}